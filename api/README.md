# API CIDENET EMPLOYEES

## About

This is a API to register employees of CIDENET .

## Available Scripts

## Run the API (is necessary to consume the data)

In the API folder, you can run:

### `npm i`

install the modules

### `npm run dev`

run the project

Open [http://localhost:4000](http://localhost:4000) ready to view it in the browser or consume.

## Tecnologías

🖥 TYPESCRIPT
🖥 VUE.JS
🖥 NODE.JS
🖌 CSS
🖌 HTML

#StartedaNewPathAsDeveloper ⌨️ with ❤️ for Angel Arrieta [@anuidev8](https://github.com/anuidev8)
