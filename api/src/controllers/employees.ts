import { Request,Response,NextFunction } from 'express'
import Employees from '../models/employees';

export const getEmployees = async(req:Request, res:Response, next:NextFunction) => {
  try{
    const employees =await Employees.find()
    res.status(200).json(employees);
  }catch(err){
    res.status(500).json({
      message:`An error ocurred ${err}`
    })
    next(err);
  }
}
export const getEmployeesByEmail = async(req:Request, res:Response, next:NextFunction) => {
  try{
    const body = req.body;
    const findEmployeeByEmail  = await Employees.findOne({email:body.email})
      if(findEmployeeByEmail){
        return res.status(200).json({type:'EMAIL',res:true});
      }
      return res.status(200).json({ok:true});
  }catch(err){
    res.status(500).json({
      message:`An error ocurred ${err}`
    })
    next(err);
  }
}
export const addEmployees = async(req:Request, res:Response, next:NextFunction) => {
  try{
    const body = req.body;  
    const findEmployeeByIdNumber  = await Employees.findOne({idNumber:body.idNumber,idType:body.idType})
    if(findEmployeeByIdNumber){
      return res.status(200).json({type:'VALIDATE',res:'User has been registered'});
    }else{
      
       const employee = await Employees.create(body) 
       return res.status(200).json({ok:true});

      
    }  
    
   
  }catch(err){
    res.status(500).json({
      message:`An error ocurred ${err}`
    })
    next(err);
  }
}
export const updateEmployees = async(req:Request, res:Response, next:NextFunction) => {
  try{
    let { id }  = req.params;
    let update = req.body;
    await Employees.findByIdAndUpdate(id,update,{new:true})
    res.status(200).json({message: 'Employees update'});
  }catch(err){
    res.status(500).json({
      message: `An error ocurred ${err}`
    })
    next(err);
  }
}
export const deleteEmployees = async(req:Request, res:Response, next:NextFunction) => {
  try {
    let { id } = req.params;
    await Employees.findByIdAndDelete(id);
    res.status(200).json({
      message: 'Question deleted'
    })
  }catch(err){
    res.status(500).json({
      message: `An error ocurred ${err}`
    })
    next(err);
  }
}