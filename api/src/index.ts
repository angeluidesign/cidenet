import cors from 'cors';
import express from 'express';
import * as dotenv from "dotenv";
import mongoose from 'mongoose';

import Employees from './routes/employees'

dotenv.config();


 
 const uri: string = "mongodb+srv://shurakd8:parzival-13@cluster0.2bv2cfy.mongodb.net/?retryWrites=true&w=majority";

mongoose.connect(uri, (err: any) => {
  if (err) {
    console.log(err.message);
   } else {
      console.log(`Connecting to MONGO`);
   }
}); 


 const PORT: number = 4000;
 
 const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));


//routes
const authorisedRoute = express.Router();
app.use("/api/employees", Employees);


// Send message for default URL
authorisedRoute.get('/', (req, res) => res.send('Welcome to default response of Product API'));


 app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});


