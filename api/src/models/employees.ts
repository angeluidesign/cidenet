import mongoose, { Schema } from 'mongoose'
/* mongoose-unique-validator */
// let validQuestions = {
//   values: ['QUESTION_OPEN','MULTIPLE'],
//   message: '{VALUE} is not a valid question' 
// }

enum IdTypes{
   CC='cc',
   CCEX='ccex',
   PASSPORT='passport',
   ESPECIAL='especial'
}
enum Countries{
   CO='co',
   US='us',
  
}

export interface Employees extends mongoose.Document{
   
      firstName:string
      firstLastName:string
      secondLastName:string
      others:string

   country:Countries
   idType:IdTypes
   idNumber:string
   email:string
   area:string
   createAt: Date;
   registrationDate:string
   state:boolean;
}


const EmployeesSchema = new Schema({
      firstName: {type:String},
      firstLastName:  {type:String},
      secondLastName:  {type:String},
      others: {type:String},
   country:{type:String},
   idType:{type:String},
   idNumber:{type:String},
   email:{type:String},
   area:{type:String},
   registrationDate:{type:String},
   createAt:    {type:Date, default: Date.now()},
   state:       {type:Boolean, default:true}
},{versionKey:false});

const Question = mongoose.model<Employees>('Employees',EmployeesSchema);
export default Question;