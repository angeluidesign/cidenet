import { Router } from 'express'

import {
    addEmployees,
    getEmployees,
    updateEmployees,
    deleteEmployees,
    getEmployeesByEmail
} from '../controllers/employees';


const router: Router = Router();

router.get('/',getEmployees)
router.post('/confirm-email',getEmployeesByEmail)
router.post('/register',addEmployees)
router.put('/update/:id',updateEmployees)
router.delete('/delete',deleteEmployees)


export default router;