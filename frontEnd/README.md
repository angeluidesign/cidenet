# REGISTER CIDENET EMPLOYEES

## About

This is a web platform to register employees of CIDENET .

## Demo

[![Demo](https://drive.google.com/file/d/1huMePES-Vp5rsrUMhv3Xwu0u3pNxJe2R/view?usp=sharing)](https://drive.google.com/file/d/1fCPFrfxtIpMPE_kwdAm5TfvHFytMIid-/view?usp=sharing)

## Available Scripts

## Run the project

In the frontEnd folder, you can run:

### `npm i`

install the modules

### `npm run dev`

run the project
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Tecnologías

🖥 TYPESCRIPT
🖥 VUE.JS
🖥 NODE.JS
🖌 CSS
🖌 HTML

#StartedaNewPathAsDeveloper ⌨️ with ❤️ for Angel Arrieta [@anuidev8](https://github.com/anuidev8)
