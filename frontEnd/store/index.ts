import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { Employee } from '@/models/employees'

export const state = () => ({

  employees:[] as Employee[],
  isLoadingEmployees:false as boolean,
  isEditingEmployee:false as boolean,
  employee:{} as Employee,
  registerMessage:{
    type:null,
    message:null
  }
})

export type RootState = ReturnType<typeof state>


export const mutations: MutationTree<RootState> = {
  SET_EMPLOYEES: (state, employees: Employee[]) => (state.employees = employees),
  SET_EMPLOYEE: (state, employee: Employee) => (state.employee = employee),
  SET_IS_LOADING_EMPLOYEES: (state) => (state.isLoadingEmployees = !state.isLoadingEmployees),
  SET_IS_EDITING_EMPLOYEE: (state) => (state.isEditingEmployee = !state.isEditingEmployee),
  SET_REGISTER_MESSAGE: (state,message) => (state.registerMessage = {type:message.type,message}),
}

export const actions: ActionTree<RootState, RootState> = {
  async employeeRegister({ commit },employee:Employee) {
    try {
      const {type,res} = await this.$axios.$post('/register',employee)
      switch (type) {
        case 'VALIDATE':
          commit('SET_REGISTER_MESSAGE',{type,message:res})
          break;

        case 'EMAIL':
            commit('SET_REGISTER_MESSAGE',{type,message:res})
            break;

        default:
          commit('SET_REGISTER_MESSAGE',null)
          break;
      }
    } catch (error) {


    }

  },
  async getEmployeeByEmail({ commit },employee:Employee) {
    try {
      const {type,res} = await this.$axios.$post('/confirm-email',employee)
      commit('SET_REGISTER_MESSAGE',{type,message:res})
    } catch (error) {


    }

  },
  async getEmployees({ commit }) {

    commit('SET_IS_LOADING_EMPLOYEES')
    const employees:Employee = await this.$axios.$get('/')
    commit('SET_EMPLOYEES',employees)
    commit('SET_IS_LOADING_EMPLOYEES')

  },
  async editEmployees({ commit },{id}) {
    const res = await this.$axios.$put(`/update/${id.id}`,id.employee)



  },
}
