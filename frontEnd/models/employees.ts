
enum IdTypes{
  CC='cc',
  CCEX='ccex',
  PASSPORT='passport',
  ESPECIAL='especial'
}

export enum Countries{
  CO='co',
  US='us',

}

export interface Employee{
  _id?:string
  firstName:string
  firstLastName:string
  secondLastName:string
  others:string

country:string
idType:IdTypes
idNumber:string
email:string
area:string
createAt: Date;
registrationDate:string
state:boolean;
}
