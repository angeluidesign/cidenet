# REGISTER CIDENET EMPLOYEES

## About

This is a web platform to register employees of CIDENET .

## Demo

[![Demo](https://drive.google.com/file/d/1huMePES-Vp5rsrUMhv3Xwu0u3pNxJe2R/view?usp=sharing)](https://drive.google.com/file/d/1fCPFrfxtIpMPE_kwdAm5TfvHFytMIid-/view?usp=sharing)

## Available Scripts

## First, run the API (is necessary to consume the data)

In the API folder, you can run:

### `npm i`

install the modules

### `npm run dev`

run the project

Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

## Second, run the FrontEnd project

In the frontEnd folder, you can run:

### `npm i`

install the modules

### `npm run dev`

run the project
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Tecnologías

🖥 TYPESCRIPT
🖥 VUE.JS
🖥 NODE.JS
🖥 MONGODB
🖌 CSS
🖌 HTML

#StartedaNewPathAsDeveloper ⌨️ with ❤️ for Angel Arrieta [@anuidev8](https://github.com/anuidev8)
